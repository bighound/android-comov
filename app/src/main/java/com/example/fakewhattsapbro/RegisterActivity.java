package com.example.fakewhattsapbro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fakewhattsapbro.apiTools.APIUtils;
import com.example.fakewhattsapbro.dataModel.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private EditText user, email, pass, repass;
    private CheckBox isRobot, acceptTerms;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        user = findViewById(R.id.usuario);
        email = findViewById(R.id.email);
        pass = findViewById(R.id.password);
        repass = findViewById(R.id.rePassword);

        isRobot = findViewById(R.id.checkBox);
        acceptTerms = findViewById(R.id.checkBox2);
    }

    public void register(View v){
        String usuario = user.getText().toString().trim();
        String correo = email.getText().toString().trim();
        String password = pass.getText().toString().trim();
        String repassword = repass.getText().toString().trim();


        if(usuario.isEmpty()){
            user.setError(getString(R.string.filledWarning));
            user.requestFocus();
            return;
        }

        if(correo.isEmpty()){
            email.setError(getString(R.string.filledWarning));
            email.requestFocus();
            return;
        }

        if(password.isEmpty()){
            pass.setError(getString(R.string.filledWarning));
            pass.requestFocus();
            return;
        }

        if(!repassword.equals(password)){
            repass.setError("The password must match");
            return;
        }

        if(!isRobot.isChecked()){
            isRobot.setError("It must not be a robot");
            return;
        }

        if(!acceptTerms.isChecked()){
            acceptTerms.setError("You must accept the terms and conditions");
            return;
        }

        Call<User> call = APIUtils
                .getAPIService()
                .registro(new User(usuario, correo, password));
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful() && response.code() == 201){
                    Toast.makeText(RegisterActivity.this,
                            "The user has been successfully registered", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(RegisterActivity.this,
                            "User already exists", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(RegisterActivity.this,
                        "Unable to register the user, contact the administrator", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void cleanScreen(View v){
        user.setText("");
        email.setText("");
        pass.setText("");
        repass.setText("");
        isRobot.setChecked(false);
        acceptTerms.setChecked(false);
    }
}
