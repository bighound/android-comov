package com.example.fakewhattsapbro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.example.fakewhattsapbro.dataModel.User;
import com.google.gson.Gson;

public class LoginSuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Cargar shared preferences
        SharedPreferences mSharedPreferences = getSharedPreferences("SPREF", 0);

        // Comprobar si está el usuario
        if(mSharedPreferences.contains("User")) {
            User user = new Gson().fromJson(mSharedPreferences.getString("User", ""), User.class);
            Log.i("Logged user", user.getName());
        }

    }


}
