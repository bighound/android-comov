package com.example.fakewhattsapbro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fakewhattsapbro.apiTools.APIUtils;
import com.example.fakewhattsapbro.dataModel.LoginInformation;
import com.example.fakewhattsapbro.dataModel.UserLogged;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private EditText user, pass;

    // Almacenar usuario para que esté disponible en el resto de actividades
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor prefsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user = findViewById(R.id.editText);
        pass = findViewById(R.id.editText2);

        // Shared preferences - save token
        mSharedPreferences = getApplicationContext().getSharedPreferences("SPREF", 0);
    }

    public void login(View v){
        final String username = user.getText().toString().trim();
        String password = pass.getText().toString().trim();

        if(username.isEmpty()){
            user.setError(getString(R.string.filledWarning));
            user.requestFocus();
            return;
        }

        if(password.isEmpty()){
            pass.setError(getString(R.string.filledWarning));
            pass.requestFocus();
            return;
        }

        Call<UserLogged> call = APIUtils
                .getAPIService()
                .inicioSesion(new LoginInformation(username, password));

        call.enqueue(new Callback<UserLogged>() {
            @Override
            public void onResponse(Call<UserLogged> call, Response<UserLogged> response) {
                String s;
                String token;
                if(response.isSuccessful() && response.code() == 200){
                    s = "User " + username + " has successfully logged in";
                    UserLogged userLogged = response.body();
                    token = userLogged.getToken();
                    s = s+" with token: "+ token;

                    Log.i("User", userLogged.getUser().getName());
                    // Guardar usuario
                    prefsEditor = mSharedPreferences.edit();
                    prefsEditor.putString("User", new Gson().toJson(userLogged.getUser()));
                    prefsEditor.apply();

                    Intent intent = new Intent(MainActivity.this, LoginSuccessActivity.class);
                    startActivity(intent);

                } else{
                    s = "The username/password is incorrect";
                }
                Toast.makeText(MainActivity.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<UserLogged> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    public void register(View v){
        Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
        startActivity(intent);
    }


}
