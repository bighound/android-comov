package com.example.fakewhattsapbro.apiTools;

public class APIUtils {

    private APIUtils() {}

    public static final String BASE_URL = "http://192.168.31.185:3800/api/";

    public static APIService getAPIService() {

        return APIAdapter.getClient(BASE_URL).create(APIService.class);
    }
}