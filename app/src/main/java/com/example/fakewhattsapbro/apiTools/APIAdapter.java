package com.example.fakewhattsapbro.apiTools;

import android.util.Log;

import com.example.fakewhattsapbro.dataModel.UserLogged;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIAdapter {

    private static Retrofit retrofit = null;
    private static String token;

    // Authentication with OkHttp interceptors - modify header in each request except login request
    private static OkHttpClient interceptRequest() {
        return new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                // Original request
                Request original = chain.request();

                // Check if login request
                if (original.url().encodedPath().equals("/api/login")) {
                    // Get token after login
                    Response response = chain.proceed(original);
                    // peekBody makes a copy that you are allowed to read
                    // https://stackoverflow.com/questions/44880303/okhttp-3-illegalstateexception
                    ResponseBody resBody = response.peekBody(Long.MAX_VALUE);
                    if (response.isSuccessful() && response.code() == 200) {
                        token = (new Gson().fromJson(resBody.string(), UserLogged.class)).getToken();
                        Log.i("Intercepted response", token);
                    }
                    return response;
                } else {
                    // Modified request with Authorization token
                    Request.Builder builder = original.newBuilder().header("Authorization", token);

                    Request newRequest = builder.build();
                    Log.i("Modified request", newRequest.headers().toString());
                    return chain.proceed(newRequest);
                }
            }

        }).build();
    }

    public static Retrofit getClient(String baseUrl) {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(interceptRequest())
                    .build();
        }
        return retrofit;
    }
}

