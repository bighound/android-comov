package com.example.fakewhattsapbro.apiTools;

import com.example.fakewhattsapbro.dataModel.LoginInformation;
import com.example.fakewhattsapbro.dataModel.User;
import com.example.fakewhattsapbro.dataModel.UserLogged;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {

 @POST("login")
 @Headers("Content-Type: application/json")
 Call<UserLogged> inicioSesion(@Body LoginInformation loginInformation);

 @POST("user")
 @Headers("Content-Type: application/json")
 Call<User> registro(@Body User user);
}



