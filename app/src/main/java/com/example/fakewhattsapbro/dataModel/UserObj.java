package com.example.fakewhattsapbro.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserObj {

    @SerializedName("userObj")
    @Expose
    private User userObj;

    public User getUser() {
        return userObj;
    }

    public void User(User userObj) {
        this.userObj = userObj;
    }
}