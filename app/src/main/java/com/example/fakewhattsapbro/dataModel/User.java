package com.example.fakewhattsapbro.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("pHash")
        @Expose
        private String pHash;
        @SerializedName("image")
        @Expose
        private String image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPHash() {
            return pHash;
        }

        public void setPHash(String pHash) {
            this.pHash = pHash;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String toString(){
            return "Usuario: " + this.name + "Password: " + this.pHash;
        }

        public User(String usuario, String password){
            this.name = usuario;
            this.pHash = password;
        }

        public User(String usuario, String email, String password){
            this.name = usuario;
            this.email = email;
            this.pHash = password;
            this.image = null;      // Aquí debería de verse una imagen en base 64

        }


    }

