package com.example.fakewhattsapbro.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLogged {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("user")
    @Expose
    private User user = null;

    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }

    public void User(User user) {
        this.user = user;
    }

}